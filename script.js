class LogicAcceptor {
	index;
	specialSymbols = ['(', ')', '¬', '!', 'Λ', '&', 'ν', '|', '⇔', '=', '⇒', '>', '⊕'];
	inputText;
	nameList;
	sourceList;
	depth;
	/**
	 *
	 * @param {object} inputField
	 * @param {object} logicDisplay
	 * @param {object} logicValuesParent
	 * @param {object} trueOutput
	 * @param {object} falseOutput
	 */
	constructor(inputField, logicDisplay, logicText, logicValuesParent, trueOutput, falseOutput) {
		this.trueOutput = trueOutput;
		this.falseOutput = falseOutput;
		this.inputField = inputField;
		this.logicDisplay = logicDisplay;
		this.logicText = logicText;
		this.logicValuesParent = logicValuesParent;

		this.trueOutput.hide();
		this.falseOutput.hide();
	}

	generateVariables() {
		this.inputText = this.inputField.val();
		this.inputField.val('');

		this.logicText.html(this.inputText);
		this.logicValuesParent.html('');
		if (this.inputText.length === 0) {
			this.badData();
		}
		var symbols = [];
		for (var i = 0; i < this.inputText.length; i++) {
			var c = this.inputText[i];
			if (!this.specialSymbols.includes(c) && !symbols.includes(c)) symbols.push(c);
		}

		var possibilities = this.generatePossibities(symbols);
		this.showPossibilities(possibilities);
		this.displayVariables(symbols);
		this.startCompute();
	}

	generatePossibities(symbols) {
		var possibilities = [];
		var row = [];
		var rowVal = [];
		for (var i = 0; i < symbols.length; i++) {
			row.push(symbols[i]);
			rowVal.push(0);
		}
		possibilities.push(row);
		possibilities.push(rowVal);

		for (var i = 1; i < Math.pow(2, symbols.length); i++) {
			var row = [];
			var previousRow = possibilities[i];
			var display;
			for (var j = 0; j < symbols.length; j++) {
				var modulo = i % Math.pow(2, j);
				if (modulo != 0) {
					display = previousRow[j];
				} else {
					if (previousRow[j] == 0) {
						display = 1;
					} else {
						display = 0;
					}
				}
				row.push(display);
			}
			possibilities.push(row);
		}
		return possibilities;
	}
	showPossibilities(possibilities) {
		this.logicDisplay.html('');
		var table = $('<table></table>');
		for (let i = 0; i < possibilities.length; i++) {
			var displayRow = $('<tr></tr>');
			for (let j = 0; j < possibilities[i].length; j++) {
				var displayVal = $('<td></td>');
				const value = possibilities[i][j];
				displayVal.append(value);
				displayRow.append(displayVal);
			}

			if (i === 0) {
				///diplay not values
				var resultVal = $('<td id="out"></td>');
				resultVal.append('out');
				displayRow.append(resultVal);
				table.append(displayRow);
				this.nameList = possibilities[0];
			} else {
				///values
				var resultVal = $('<td id="out"></td>');

				//console.log(this.nameList, this.sourceList);
				this.index = 0;
				this.sourceList = possibilities[i];
				var v = this.compute();
				resultVal.append(v);

				if (v === 1) {
					resultVal.addClass('ok');
				} else {
					resultVal.addClass('bad');
				}

				displayRow.append(resultVal);
				table.append(displayRow);
			}
		}
		this.logicDisplay.append(table);
	}

	displayVariables(symbols) {
		for (var i = 0; i < symbols.length; i++) {
			var symbol = symbols[i];
			var checkbox = $('<input id="' + symbol + '" type="checkbox">');
			var checkmark = $('<span class="checkmark"></span>');
			var label = $('<label class="container"></label>');

			label.append(checkbox);
			label.append(symbol);
			label.append(checkmark);

			checkbox.change({ caller: this }, function (event) {
				event.data.caller.startCompute();
			});

			this.logicValuesParent.append(label);
		}
	}

	startCompute() {
		this.sourceList = undefined;
		this.index = 0;
		this.depth = 0;
		var o = this.compute();
		if(this.depth === 0){
			if (o === 1) {
				this.trueOutput.show();
				this.falseOutput.hide();
			} else if(o===0){
				this.trueOutput.hide();
				this.falseOutput.show();
			}
		}else{
			this.badData();
		}
	}
	/**
	 * @returns {boolean}
	 */
	compute() {
		var currentEquationResult = undefined;
		var currentSymbol;

		var expectValue = false;
		var expectSymbol = false;
		for (; this.index < this.inputText.length; this.index++) {
			var c = this.inputText[this.index];
			switch (c) {
				case ' ':
					continue;
				case '(':
					// příklad:  a(     jsme po a => tedy očekáváme  aΛ(
						//není-li symbol => chybná data
					if (expectSymbol === true) {
						this.badData();
					}
					expectValue = false;
					this.depth++;
					this.index++;
					//znamena spočnten obsah ()
					var innerCompute = this.compute();  //rekurze
					//pokud innerCompute nic nevrátí znamená to =>  ()
					if(innerCompute === undefined){
						this.badData();
					}
					currentEquationResult = this.resolveLogic(currentEquationResult, currentSymbol, innerCompute);
					currentSymbol = undefined;
					break;
				case ')':
					this.depth--;
					//zde mne vyhodi vnitrni compute => konec rekurze
					return currentEquationResult; 

				default:
					if (this.specialSymbols.includes(c)) {
						if (c == '¬' || c == '!') {
							this.index++;
							var reverseValueName = this.inputText[this.index];
							if (reverseValueName == '(') {
								var innerCompute = this.compute(); //znamena spočnten obsah ()
								var value = innerCompute == 1 ? 0 : 1;
								currentEquationResult = this.resolveLogic(currentEquationResult, currentSymbol, value);
								currentSymbol = undefined;
							} else if (!this.specialSymbols.includes(reverseValueName)) {
								expectSymbol = true;
								expectValue = false;

								var value = this.getValueFromName(reverseValueName) == 1 ? 0 : 1; //reversion of  normal (0:1)
								currentEquationResult = this.resolveLogic(currentEquationResult, currentSymbol, value);
								currentSymbol = undefined;
							} else {
								this.badData();
							}
						} else {
							if (expectValue === true) {
								this.badData();
							}
							expectSymbol = false;
							expectValue = true;
							currentSymbol = c;
						}
					} else {
						if (expectSymbol === true) {
							this.badData();
						}
						expectSymbol = true;
						expectValue = false;
						var value = this.getValueFromName(c);
						currentEquationResult = this.resolveLogic(currentEquationResult, currentSymbol, value);
						currentSymbol = undefined;
					}

					break;
			}
		}
		if (expectValue === true) {
			this.badData();
		}
		return currentEquationResult;
	}

	resolveLogic(leftValue, symbol, rightValue) {
		if (leftValue === undefined || symbol === undefined)
			// zapisujeme první value
			return rightValue;
		else
			switch (symbol) {
				case 'Λ':
				case '&':
					if (leftValue == 1 && rightValue == 1) return 1;
					else return 0;
				case 'ν':
				case '|':
					if (leftValue == 1 || rightValue == 1) return 1;
					else return 0;
				case '⇔':
				case '=':
					if (leftValue === rightValue) return 1;
					else return 0;
				case '⇒':
				case '>':
					if (leftValue == 1)
						if (rightValue == 1) {
							return 1;
						} else {
							return 0;
						}
					else return 1;
				case '⊕':
					if (leftValue !== rightValue) return 1;
					else return 0;
			}
	}

	badData() {
		this.logicValuesParent.html('');
		this.logicDisplay.html('');

		this.trueOutput.hide();
		this.falseOutput.hide();
		this.depth = null;
		throw new Error('Zadané data nejsou správná');
	}

	getValueFromName(name) {
		if (this.sourceList === undefined) {
			return $('#' + name).is(':checked') ? 1 : 0;
		} else {
			var value = this.nameList.findIndex((val) => val === name);
			return this.sourceList[value];
		}
	}
}

class TextInsertor {
	/**
	 *
	 * @param {object} outputTo
	 * @param {string} outputText
	 */
	constructor(outputTo, outputText) {
		this.outputTo = outputTo;
		this.outputText = outputText;
	}
	insertAtCursor() {
		var cursorPos = this.outputTo.prop('selectionStart');
		var v = this.outputTo.val();
		var textBefore = v.substring(0, cursorPos);
		var textAfter = v.substring(cursorPos, v.length);

		this.outputTo.val(textBefore + this.outputText + textAfter);
		this.textSelect(document.getElementById(this.outputTo.attr('id')), textBefore.length + 1);
	}

	textSelect(inp, s, e) {
		e = e || s;
		if (inp.createTextRange) {
			var r = inp.createTextRange();
			r.collapse(true);
			r.moveEnd('character', e);
			r.moveStart('character', s);
			r.select();
		} else if (inp.setSelectionRange) {
			inp.focus();
			inp.setSelectionRange(s, e);
		}
	}
}

$(document).ready(function () {
	const logicAcceptor = new LogicAcceptor($('#logicField'), $('#logicDisplay'), $('.logicText'), $('#variables'), $('#true'), $('#false'));

	$('#setLogic').click(function (e) {
		e.preventDefault();
		logicAcceptor.generateVariables();
	});

	$('.symbol').click(function () {
		var TI = new TextInsertor($('#logicField'), $(this).text());
		TI.insertAtCursor();
	});
});
