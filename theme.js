class ThemeApplicator {
	themes = {};

	reload() {
		this.setTheme(this.getCookie());
	}

	setTheme(val) {
		if (val === null) return;
		var newTheme = this.themes[val];
		if (newTheme === undefined) return;
		this.setCookie(val);
		$('link[theme]').prop('disabled', true);
		$('link[theme]').remove();
		$('<link/>', {
			rel: 'stylesheet',
			type: 'text/css',
			href: newTheme.url,
			theme: '',
		}).appendTo('head');
		if (newTheme.onActivate != undefined) newTheme.onActivate.call();
	}

	/** Adds Theme */
	addTheme(themeName, themeURL, onActivate) {
		this.themes[themeName] = { url: themeURL, onActivate: onActivate };
	}

	/**
	 *
	 * @param {string} themeName
	 * @param {URL} themeURL
	 * @param {function} onActivate
	 */
	addTheme(themeName, themeURL, onActivate) {
		this.themes[themeName] = { url: themeURL, onActivate: onActivate };
	}

	setCookie(cvalue) {
		var d = new Date();
		d.setTime(d.getTime() + 100 * 24 * 60 * 60 * 1000);
		var expires = 'expires=' + d.toUTCString();
		document.cookie = 'theme=' + cvalue + ';' + expires + ';path=/';
	}
	getCookie() {
		var name = 'theme=';
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				var cookie = c.substring(name.length, c.length);
				if (cookie == '') {
					return null;
				} else return cookie;
			}
		}
		return null;
	}
}
